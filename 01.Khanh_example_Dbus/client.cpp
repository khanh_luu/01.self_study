#include <pthread.h>
#include "gen/example.h"
#include "client.h"

//namespace: example_ns
//interface: khanh.example.iface


GMainLoop *Client::_gLoop = NULL;

Client::Client()
{
	conn = NULL;
	_proxy = NULL; // Interface between server & client 
}

Client::~Client()
{
}

void Client::init()
{
	GError *err = NULL;
	conn = g_bus_get_sync(G_BUS_TYPE_SESSION, NULL, &err);
	_gLoop = g_main_loop_new(NULL, FALSE); 
	_proxy = example_ns_khanh_example_iface_proxy_new_sync(conn,
								G_DBUS_PROXY_FLAGS_NONE,
								"khanh.example.service",
								"/khanh/example/object/path",
								NULL,&err);
	if (err) {
		g_print("Error %s\n", err->message);
		g_error_free(err);

	}

	g_signal_connect(_proxy,"khanhsignal", G_CALLBACK(handle_khanhsignal),NULL);

	g_print("Initialized.\n");
}

void Client::uinit()
{
	g_main_loop_quit(_gLoop);
	g_main_loop_unref(_gLoop);
	_gLoop = NULL;
}

void Client::start()
{
	pthread_t thread1;
	pthread_create(&thread1,NULL, startGLoop, (void*)NULL);
	g_print("Client started.\n");
}

void* Client::startGLoop(void *args)
{
	g_main_loop_run(_gLoop);
	return NULL;
}

int Client::requestOne(int n)
{
	 gint in = (gint)n;
	 gint out;
	 GError *err = NULL;
	 example_ns_khanh_example_iface_call_add_one_sync(_proxy, in, &out, NULL, &err);
	 if (err) {
		 g_print("%s", err->message);
		 uinit(); // Uninit 
	 }

	 return (int)out;
}

int Client::requestSum(int a, int b)
{
	 gint in1 = (gint)b;
	 gint in2 = (gint)a;
	 gint out;
	 GError *err = NULL;
	 example_ns_khanh_example_iface_call_sum_sync(_proxy, in1, in2, &out, NULL, &err);
	 if (err) {
		 g_print("%s", err->message);
		 uinit(); // Uninit 
	 }

	 return (int)out;
}
gboolean Client::handle_khanhsignal(examplensKhanhExampleIface *object, gint in_arg)
{
	g_print("Received mysignal from server with value %d.\n", in_arg);
	return TRUE;
}

int main()
{
	Client client;
	client.init();
	client.start();

	int n = 2;
	int a = 4;
	int b = 5;
	g_print ("Sending [%d]. \n",n);
	int result = client.requestOne(n);
	g_print("Got [%d] from server\n", result);

	int sum = client.requestSum(a,b);
	g_print("Got [%d] from server\n", sum);
	while (1) {
		sleep(1);
	}

	return 0;
}
