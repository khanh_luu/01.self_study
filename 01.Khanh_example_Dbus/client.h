#include "gen/example.h"

class Client {
public:
	Client();
	~Client();
	void init();
	void start();
	void uinit();
	int requestOne(int n);
	int requestSum(int a, int b);
private:
	GDBusConnection *conn;
	static GMainLoop *_gLoop;
	examplensKhanhExampleIface *_proxy;

	static void *startGLoop(void*);
	static gboolean handle_khanhsignal(examplensKhanhExampleIface *object, gint in_arg);
};
