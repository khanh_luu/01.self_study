#include <pthread.h>
#include "server.h"

//Interface=khanh.example.iface
//Namespace=example_ns
//Abstract DBUS interface object=examplensKhanhExampleIface

GMainLoop *Server::_gLoop = NULL;
examplensKhanhExampleIface *Server::_gSkeleton=NULL;

Server::Server()
{
}

Server::~Server()
{
}

void Server::init()
{
	(void) g_bus_own_name(G_BUS_TYPE_SESSION,
			"khanh.example.service",
			G_BUS_NAME_OWNER_FLAGS_NONE,
			&bus_ac_cb,
			&name_ac_cb,
			&name_lost_cb,
			NULL,
			NULL);
	g_print("Owned name.\n"); 
	_gLoop = g_main_loop_new(NULL, FALSE);
}

void Server::uinit()
{
	g_main_loop_quit(_gLoop);
	g_main_loop_unref(_gLoop);
	_gLoop = NULL;
}

void Server::name_ac_cb(GDBusConnection *conn, const gchar *name, gpointer user_data)
{
}

void Server::name_lost_cb(GDBusConnection *conn, const gchar *name, gpointer user_data)
{
}

void Server::bus_ac_cb(GDBusConnection *conn, const gchar *name, gpointer user_data)
{
	GError *err = NULL;
	_gSkeleton = example_ns_khanh_example_iface_skeleton_new();
	g_signal_connect(_gSkeleton, "handle-add-one", G_CALLBACK(addOne), NULL); 
	g_signal_connect(_gSkeleton, "handle-sum", G_CALLBACK(sum), NULL); 
	g_dbus_interface_skeleton_export(G_DBUS_INTERFACE_SKELETON(_gSkeleton),
					conn,
					"/khanh/example/object/path",
					&err);
	if (err !=NULL) {
		g_print("Problem exporting skel %s.", err->message);
		g_main_loop_quit(_gLoop);
	}
	g_print("Exported Skeleton.\n");
}

void Server::start()
{
	pthread_t thread1;
	int err = pthread_create(&thread1, NULL, startGLoop, (void*)NULL);
	if (err) {
		g_print("pthread creation error.\n");
		uinit();
	}
}

void* Server::startGLoop(void *args)
{
	g_main_loop_run(_gLoop);
	return NULL;
}

void Server::sendKhanhsignal()
{
	gint in = 777;

	g_print("Sending mysignal to client with value %d.\n", in);
	example_ns_khanh_example_iface_emit_khanhsignal(_gSkeleton, in); 
}

gboolean Server::addOne(examplensKhanhExampleIface *object, GDBusMethodInvocation *invocation, gint in_arg, gpointer user_data)
{
	g_print("addOne called.\n");
	in_arg += 1;
	example_ns_khanh_example_iface_complete_add_one(object,invocation, in_arg);

	return TRUE;
}


gboolean Server::sum(examplensKhanhExampleIface *object, GDBusMethodInvocation *invocation, gint in_arg1,  gint in_arg2, gpointer user_data)
{
	g_print("sum called.\n");
	gint out_arg=in_arg1 + in_arg2;
	example_ns_khanh_example_iface_complete_sum(object,invocation, out_arg);

	return TRUE;
}
int main()
{
	Server ex_server;
	ex_server.init();
	ex_server.start();
	g_print("Started server.\n");
	sleep(5);
	while (1) {
		sleep(1);
		ex_server.sendKhanhsignal();
	}

	return 0;

}












