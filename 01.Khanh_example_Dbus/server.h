#include <gio/gio.h>
#include "gen/example.h"
//Namespace=example_ns
//Interface=khanh.example.iface

class Server {
public:
	Server();
	~Server();
	void init();
	void start();
	void sendKhanhsignal();
private:
	static void uinit();
	static void* startGLoop(void *);
	static void bus_ac_cb(GDBusConnection *conn, const gchar *busName, gpointer userData);
	static void name_ac_cb(GDBusConnection *conn, const gchar *busName, gpointer userData);
	static void name_lost_cb(GDBusConnection *conn, const gchar *busName, gpointer userData);
	static examplensKhanhExampleIface *_gSkeleton;
	static GMainLoop *_gLoop;

	static gboolean addOne(examplensKhanhExampleIface *object, GDBusMethodInvocation *invocation, gint int_arg,  gpointer user_data); 
	static gboolean sum(examplensKhanhExampleIface *object, GDBusMethodInvocation *invocation, gint int_arg1, gint int_arg2, gpointer user_data); 
};	
